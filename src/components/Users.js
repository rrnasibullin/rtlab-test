import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Grid from './Grid';
import Pagination from './Pagination';
import Modal from './Modal';

export class Users extends Component {
  columns = [
    {
      title: 'Аватар',
      field: 'avatar_url',
      el: {
        type: 'image'
      }
    }, {
      title: 'Логин',
      field: 'login',
      el: {
        type: 'text'
      }
    }, {
      title: 'Ссылка на профиль',
      field: 'html_url',
      el: {
        type: 'link'
      }
    }, {
      title: 'Репозитории',
      field: 'repos_url',
      el: {
        type: 'button',
        className: 'btn btn-primary btn-block',
        text: 'Посмотреть',
        callback: this.props.getRepos
      }
    }
  ];

  componentDidMount() {
    this.props.getUsers(0, this.props.perPage)
  }

  getPage = (direction) => {
    switch (direction) {
      case 'prev':
        if (this.props.sinceIds.length > 1)
          this.props.getUsers(this.props.sinceIds[this.props.sinceIds.length - 2], this.props.perPage);
        break;
      case 'next':
        this.props.getUsers(this.props.data[this.props.data.length - 1].id, this.props.perPage);
        break;
    }
  };

  render() {
    const {data, sinceIds, error, perPage, hideUsersError} = this.props;

    const isLast = data.length >= 0 && data.length < perPage;

    return (
      <div className="users">
        <h1>Список пользователей GitHub</h1>

        <Pagination current={sinceIds.length} isLast={isLast} getPage={this.getPage}/>

        {data.length > 0  ?
          <Grid columns={this.columns} data={data}/>
          :
          <p>Нет данных для отображения</p>
        }

        {error && <Modal type="small" handleClose={hideUsersError} header="Ошибка загрузки списка пользователей">
          <div className="errorMessage">
            {error.error ? error.error.stack : error.message}
          </div>
        </Modal>}
      </div>
    )
  }
}

Users.propTypes = {
  data: PropTypes.array.isRequired,
  sinceIds: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  perPage: PropTypes.number,
  error: PropTypes.object,
  getUsers: PropTypes.func.isRequired,
  getRepos: PropTypes.func.isRequired,
  hideUsersError: PropTypes.func.isRequired
};