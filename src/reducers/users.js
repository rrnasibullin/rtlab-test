/*eslint-disable no-case-declarations*/ //todo Вынести логику в миддлевары и убрать настройку eslint
import * as types from "../actions/_types";

const initialState = {
  data: [],
  isFetching: false,
  error: null,
  sinceIds: [0],
  perPage: 10
};

export function usersReducer(state = initialState, action) {
  switch (action.type) {
    case types.GET_USERS_REQUEST:
      return {...state,
        isFetching: true,
        error: null
      };

    case types.GET_USERS_SUCCESS:
      return {...state,
        data: action.payload,
        isFetching: false
      };

    case types.GET_USERS_FAIL:
      return {...state,
        error: action.payload,
        isFetching: false,
      };

    case types.SET_USERS_PER_PAGE:
      return {...state,
        perPage: action.payload
      };

    case types.SET_USERS_PAGE:
      const _sinceIds = [...state.sinceIds];
      if(_sinceIds.indexOf(action.payload)>=0) {
        if(_sinceIds.length > 1) {
          _sinceIds.pop()
        }
      } else {
        _sinceIds.push(action.payload)
      }
      return {...state,
        sinceIds: _sinceIds
      };

    case types.HIDE_USERS_ERROR:
      return {...state,
        error: null
      };

    default:
      return state;
  }
}