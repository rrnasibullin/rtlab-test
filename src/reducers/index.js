import {combineReducers} from 'redux';
import {usersReducer} from './users';
import {reposReducer} from './repos';

export const rootReducer = combineReducers({
  users: usersReducer,
  repos: reposReducer
});