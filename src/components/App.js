import React, {Component} from 'react';
import UsersContainer from '../containers/UsersContainer';
import ReposContainer from '../containers/ReposContainer';
import '../../node_modules/bootstrap/dist/css/bootstrap.css';
import '../styles/App.styl';

export default class App extends Component {
  render() {
    return (
      <div className="container">
        <div className="app">
          <UsersContainer/>
          <ReposContainer/>
        </div>
      </div>
    );
  }
}