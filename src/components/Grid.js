import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Grid extends Component {
  renderHeader = () => {
    return (
      <thead className="thead-dark">
      <tr>
        {this.props.columns.map((column, index) => {
          return (
            <th key={`column_${index}`}>{column.title}</th>
          )
        })}
      </tr>
      </thead>
    )
  };

  renderBody = () => {
    return (
      <tbody>
      {this.renderRows()}
      </tbody>
    )
  };

  renderRows = () => {
    return this.props.data.map((row, index) => {
      return this.renderRow(row, index)
    })
  };

  renderRow = (row, rowIndex) => {
    return (
      <tr key={row.id}>
        {this.renderCells(row, rowIndex)}
      </tr>
    )
  };

  renderCells = (row, rowIndex) => {
    return this.props.columns.map((column, index) => {
      return this.renderCell(index, column, row, rowIndex)
    })
  };

  renderCell = (cellIndex, column, row, rowIndex) => {
    return (
      <td key={row.id + '_' + cellIndex} style={column.style}>{this.renderCellContent(column.el, row[column.field], rowIndex)}</td>
    )
  };

  renderCellContent = (el, value, rowIndex) => {
    switch (el.type) {
      case 'numbering':
        return rowIndex + 1;
      case 'image':
        return <div className="img"><img className={el.className || ''} src={value} onClick={this.handleClick(el.callback, value)} alt={value}/></div>;
      case 'link':
        return <a className={el.className || ''} href={value} onClick={this.handleClick(el.callback, value)} rel="noopener noreferrer" target="_blank">{el.text || value}</a>;
      case 'button':
        return <button className={el.className || ''} onClick={this.handleClick(el.callback, value)}>{el.text || value}</button>;
      case 'check':
        return <input type="checkbox" checked={value} className={el.className || ''} readOnly/>;
      default:
        return value
    }
  };

  handleClick = (cb, v) => {
    return e => {
      if(cb && typeof cb === 'function') {
        e.preventDefault();
        cb(v);
      }
    }
  };

  render() {
    return (
      <table className="table">
        {this.renderHeader()}
        {this.renderBody()}
      </table>
    );
  }
}

Grid.propTypes = {
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired
};

export default Grid;