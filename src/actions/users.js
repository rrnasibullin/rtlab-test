import * as types from './_types';

export function getUsers(since, perPage) {
  return dispatch => {
    dispatch({
      type: types.GET_USERS_REQUEST
    });
    fetch(`https://api.github.com/users?since=${since}&per_page=${perPage}`)
      .then((res) => res.json())
      .then((data) => {
        if (Array.isArray(data)) {
          dispatch({
            type: types.GET_USERS_SUCCESS,
            payload: data
          });

          dispatch({
            type: types.SET_USERS_PAGE,
            payload: since
          })
        } else {
          dispatch({
            type: types.GET_USERS_FAIL,
            payload: {
              message: data.message
            }
          });
        }
      })
      .catch((error) => {
        dispatch({
          type: types.GET_USERS_FAIL,
          payload: {
            error: error
          }
        });
      });
  }
}

export function hideUsersError() {
  return dispatch => {
    dispatch({
      type: types.HIDE_USERS_ERROR
    });
  }
}