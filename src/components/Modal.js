import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class Modal extends Component {
  render() {
    const {children, header, handleClose, type} = this.props;
    return (
      <div className="overlay" onClick={handleClose}>
        <div className={`modal${type ? ` modal--${type}`: ''}`}>
          <span className="modal__close" onClick={handleClose}>х</span>
          {header && <div className="modal__header">{header}</div> }
          <div className="modal__inner">
            {children}
          </div>
        </div>
      </div>
    )
  }
}

Modal.propTypes = {
  header: PropTypes.string,
  type: PropTypes.string,
  children: PropTypes.any,
  handleClose: PropTypes.func.isRequired
};