import React, {Component} from 'react';

export default class Loader extends Component {
  render() {
    return (
      <div className="loader overlay">
        <div className="loader__ellipsis">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    )
  }
}