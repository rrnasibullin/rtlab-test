import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Grid from '../components/Grid';
import Loader from '../components/Loader';
import Modal from '../components/Modal';

export class Repos extends Component {
  columns = [{
    title: 'N п/п',
    field: '',
    el: {
      type: 'numbering'
    },
    style: {
      width: 100
    }
  }, {
    title: 'Описание',
    field: 'description',
    el: {
      type: 'text'
    }
  }, {
    title: 'форк',
    field: 'fork',
    el: {
      type: 'check'
    }
  }, {
    title: 'Ссылка на репозиторий',
    field: 'html_url',
    el: {
      type: 'link'
    }
  }];

  render() {
    const {show, data, isFetching, error, hideRepos, hideReposError} = this.props;

    return (
      <>
        {show && <Modal handleClose={hideRepos} header="Список репозиториев">
          <div className="repos">
            <Grid columns={this.columns} data={data}/>
          </div>
        </Modal>}

        {error && <Modal type="small" handleClose={hideReposError} header="Ошибка загрузки списка репозиториев">
          <div className="errorMessage">
            {error.error ? error.error.stack : error.message}
          </div>
        </Modal>}

        {isFetching && <Loader/>}
      </>
    )
  }
}

Repos.propTypes = {
  show: PropTypes.bool.isRequired,
  data: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  hideRepos: PropTypes.func.isRequired,
  hideReposError: PropTypes.func.isRequired,
  error: PropTypes.object
};