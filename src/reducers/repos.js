import * as types from '../actions/_types';

const initialState = {
  data: [],
  isFetching: false,
  error: null,
  show: false
};

export function reposReducer(state = initialState, action) {
  switch (action.type) {
    case types.GET_REPOS_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: null
      };

    case types.GET_REPOS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        isFetching: false
      };

    case types.GET_REPOS_FAIL:
      return {
        ...state,
        error: action.payload,
        isFetching: false,
      };

    case types.SHOW_REPOS:
      return {
        ...state,
        show: true
      };

    case types.HIDE_REPOS:
      return {
        ...state,
        show: false
      };

    case types.HIDE_REPOS_ERROR:
      return {...state,
        error: null
      };

    default:
      return state;
  }
}