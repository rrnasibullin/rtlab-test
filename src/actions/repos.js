import * as types from './_types';

export function getRepos(url) {
  return dispatch => {
    dispatch({
      type: types.GET_REPOS_REQUEST
    });

    fetch(`${url}?per_page=1000000`)
      .then((res) => res.json())
      .then((data) => {
        if (Array.isArray(data)) {
          dispatch({
            type: types.GET_REPOS_SUCCESS,
            payload: data
          });
          dispatch({
            type: types.SHOW_REPOS
          });
        } else {
          dispatch({
            type: types.GET_REPOS_FAIL,
            payload: {
              message: data.message
            }
          });
        }
      })
      .catch((error) => {
        dispatch({
          type: types.GET_REPOS_FAIL,
          payload: {
            error: error,
          }
        });
      });
  }
}

export function hideRepos() {
  return dispatch => {
    dispatch({
      type: types.HIDE_REPOS
    });
  }
}

export function hideReposError() {
  return dispatch => {
    dispatch({
      type: types.HIDE_REPOS_ERROR
    });
  }
}