import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class Pagination extends Component {
  prev = e => {
    e.preventDefault();
    this.props.getPage('prev');
  };
  next = e => {
    e.preventDefault();
    this.props.getPage('next');
  };

  render() {
    const {current, isLast} = this.props;

    return (
      <>
      <div className="page-number">Страница: {current}</div>
      <nav aria-label="...">
        <ul className="pagination">
          <li className={`page-item ${current === 1 ? 'disabled' : ''}`} onClick={this.prev}>
            <a className="page-link" href="#">Предыдущая</a>
          </li>

          <li  className={`page-item ${isLast ? 'disabled' : ''}`}>
            <a className="page-link" href="#" onClick={this.next}>Следующая</a>
          </li>
        </ul>
      </nav>
      </>
    )
  }
}

Pagination.propTypes = {
  current: PropTypes.number.isRequired,
  getPage: PropTypes.func.isRequired,
  isLast: PropTypes.bool.isRequired
};