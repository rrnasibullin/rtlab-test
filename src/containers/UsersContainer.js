import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Users} from '../components/Users';
import Loader from '../components/Loader';
import {getUsers, hideUsersError} from '../actions/users';
import {getRepos} from '../actions/repos';

class UsersContainer extends Component {
  render() {
    const {users, getUsersAction, getReposAction, hideUsersErrorAction} = this.props;

    return (
      <>
        <Users {...users} getUsers={getUsersAction} getRepos={getReposAction} hideUsersError={hideUsersErrorAction}/>
        {users.isFetching && <Loader/>}
      </>
    )
  }
}

const mapStateToProps = store => {
  return {
    users: store.users
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getUsersAction: (since, perPage) => dispatch(getUsers(since, perPage)),
    hideUsersErrorAction: () => dispatch(hideUsersError()),
    getReposAction: url => dispatch(getRepos(url))
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersContainer);