import React, {Component} from 'react';
import {connect} from 'react-redux'
import {Repos} from '../components/Repos';
import {hideRepos, hideReposError} from '../actions/repos';

class ReposContainer extends Component {
  render() {
    const {repos, hideReposAction, hideReposErrorAction} = this.props;
    return (
      <Repos {...repos} hideRepos={hideReposAction} hideReposError={hideReposErrorAction}/>
    )
  }
}

const mapStateToProps = store => {
  return {
    repos: store.repos
  }
};

const mapDispatchToProps = dispatch => {
  return {
    hideReposAction: () => dispatch(hideRepos()),
    hideReposErrorAction: () => dispatch(hideReposError())
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReposContainer);